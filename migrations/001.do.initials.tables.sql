/*
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
*/

BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "fuzzystrmatch";

/* ROLE BLOCK*/
CREATE TABLE public."role" (
    "id"   SERIAL , 
    "name" VARCHAR(255), 
    "uuid" UUID DEFAULT uuid_generate_v4(), 
    "hash" UUID DEFAULT uuid_generate_v4(),  

    "createdAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "updatedAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "deletedAt" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id")
);

CREATE TABLE public."permission" (
    "id"   SERIAL , 
    "code" VARCHAR(255), 
    "description" VARCHAR(255), 
    "uuid" UUID DEFAULT uuid_generate_v4(), 
    "hash" UUID DEFAULT uuid_generate_v4(),  

    "createdAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "updatedAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "deletedAt" TIMESTAMP WITH TIME ZONE,  
    PRIMARY KEY ("id")
);

CREATE TABLE public."RolePermission" (
    "id"   SERIAL ,
    "roleId" INTEGER NOT NULL,
    "permissionId" INTEGER NOT NULL,

    "createdAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "updatedAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "deletedAt" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id")
);

CREATE TABLE public."user" (
    "id"   SERIAL , 
    "login" VARCHAR(255) NOT NULL, 
    "roleId" INTEGER DEFAULT 1, 
    "password" VARCHAR(255) NOT NULL, 
    "name" VARCHAR(255), 
    "isActive" BOOLEAN DEFAULT true, 
    "email" VARCHAR(255), 
    "registerDate" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "uuid" UUID DEFAULT uuid_generate_v4(), 
    "hash" UUID DEFAULT uuid_generate_v4(), 
    "activationId" UUID DEFAULT uuid_generate_v4(),   

    "createdAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "updatedAt" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    "deletedAt" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id")
);

COMMIT;