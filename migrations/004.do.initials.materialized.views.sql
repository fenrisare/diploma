BEGIN;

CREATE MATERIALIZED VIEW public."userMv" AS
SELECT  
	u."id",
	u."login",
	u."name", 
	u."isActive", 
	u."email",
	u."registerDate", 
	u."uuid",
    u."createdAt",
    u."updatedAt",
    u."deletedAt",
	r."name" AS "roleName"
FROM "user" AS u 
    LEFT OUTER JOIN "role" AS r ON u."roleId" = r."id"
WITH DATA;

CREATE UNIQUE INDEX idx_user_mv_id
    ON public."userMv" USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE OR REPLACE FUNCTION refresh_user_mv()
    RETURNS TRIGGER LANGUAGE plpgsql
    AS $$
    BEGIN
        REFRESH MATERIALIZED VIEW CONCURRENTLY "userMv";
        RETURN NULL;
    END $$;

DROP TRIGGER IF EXISTS refresh_user_mv_trigger on "public"."user";
CREATE TRIGGER refresh_user_mv_trigger
    AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
    ON "public"."user"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE refresh_user_mv();

CREATE MATERIALIZED VIEW public."roleMv" AS
SELECT  
	r."id",
	r."name",
	r."uuid",
    r."createdAt",
    r."updatedAt",
    r."deletedAt",
    (SELECT count(id) FROM public."user" WHERE "user"."roleId" = r.id) as "usersCount"
FROM "role" AS r WITH DATA;

CREATE UNIQUE INDEX idx_role_mv_id
    ON public."roleMv" USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE OR REPLACE FUNCTION refresh_role_mv()
    RETURNS TRIGGER LANGUAGE plpgsql
    AS $$
    BEGIN
        REFRESH MATERIALIZED VIEW CONCURRENTLY "roleMv";
        RETURN NULL;
    END $$;

DROP TRIGGER IF EXISTS refresh_role_mv_trigger on "public"."role";
CREATE TRIGGER refresh_role_mv_trigger
    AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
    ON "public"."role"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE refresh_role_mv();
    
    
    
    
CREATE MATERIALIZED VIEW public."permissionMv" AS
SELECT
	p."id",
	p."code",
	p."description",
	p."uuid",
    p."createdAt",
    p."updatedAt",
    p."deletedAt"
FROM "permission" AS p WITH DATA;

COMMIT;