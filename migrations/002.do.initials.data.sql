BEGIN;

INSERT INTO public."role"("id", "name")	VALUES
    (1, 'Admin');

INSERT INTO public."permission" ("code", "description") VALUES 
	('FULL_ADMIN', 'Абсолютное право на все действия в системе'),
	('USER', 'Базовое право пользователя'),
	('ADMIN', 'Доступ к админ меню'),
	('ADMIN.USER', 'Доступ к меню пользователей'),
	('ADMIN.USER.EDIT', 'Возможность редактирования и удаления данных пользователя'),
	('ADMIN.USER.VIEW', 'Возможность просмотра данных пользователя'),
	('ADMIN.ROLE', 'Доступ к меню ролей'),
	('ADMIN.ROLE.EDIT', 'Возможность редактирования и удаления данных ролей'),
	('ADMIN.ROLE.VIEW', 'Возможность просмотра данных ролей'),
	('ADMIN.PERMISSION', 'Доступ к меню разрешений'),
	('ADMIN.PERMISSION.EDIT', 'Возможность редактирования и удаления разрешений'),
	('ADMIN.PERMISSION.VIEW', 'Возможность просмотра разрешений'),
	('ADMIN.CUSTOMER', 'Доступ к меню клиентов'),
	('ADMIN.CUSTOMER.EDIT', 'Возможность редактирования и удаления клиентов'),
	('ADMIN.CUSTOMER.VIEW', 'Возможность просмотра клиентов');

INSERT INTO public."RolePermission" ("roleId", "permissionId") 
  SELECT (SELECT id FROM "role" WHERE "name" = 'Admin') as "roleId", id as "permissionId" FROM "permission" WHERE "code" = 'FULL_ADMIN';

INSERT INTO public."user"(login, password, name, email, "roleId", uuid)	VALUES 
    ('admin', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'Big Boss', 'BigBoss@gmail.com', (SELECT id FROM "role" WHERE "role"."name" = 'Admin'), '2856486a-48c7-4943-93ac-04ea8d26a3ef');

COMMIT;