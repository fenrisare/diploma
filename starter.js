'use strict';

var exec = require('child_process').exec;

console.log('NODE_ENV = ', process.env.NODE_ENV, ' NODE_BUILD_TYPE = ', process.env.NODE_BUILD_TYPE);
if(process.env.NODE_BUILD_TYPE == 'production') {
  exec('gulp prod');
} else {
  exec('gulp');
}
