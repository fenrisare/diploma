"use strict";

let gulp = require('gulp'),
  plugins = require('gulp-load-plugins')(),
    
  defaultAssets = require('./configuration/server/assets/development'),
  config = require('./configuration/server/main'),
  lintPassed = true,
    
  del = require('del'),
  path = require('path');

// LINTERS
gulp.task('lint', () => {
  return gulp.src('modules/**/*.js')
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('jshint-stylish'));
}); 


// MINIFICATION
gulp.task('minifyLibJs', () => {
  return gulp.src(defaultAssets.client.lib.js)
    .pipe(plugins.concat('application.lib.js'))
//    .pipe(plugins.ngAnnotate())
//    .pipe(plugins.minify())
    .pipe(gulp.dest('public/dist'));
}); 

gulp.task('minifyAppJs', () => {
  let env = process.env.NODE_ENV,
    result = gulp.src(defaultAssets.client.app.js)
      .pipe(plugins.concat('application.js'))
      .pipe(plugins.ngAnnotate())
      .pipe(gulp.dest('public/dist'));
  if(env != 'development') {
    result.pipe(plugins.minify())
    .on('error', function(err) {
      console.error('Error in compress task', err.toString());
    });
  }
  
  return result;
}); 

gulp.task('minifyAppCss', () => {
  return gulp
    .src(defaultAssets.client.app.css)
    .pipe(plugins.concat('application.css'))
    .pipe(plugins.cleanCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/dist'));
}); 

gulp.task('minifyLibCss', () => {
  return gulp
    .src(defaultAssets.client.lib.css)
    .pipe(plugins.concat('application.lib.css'))
    .pipe(plugins.cleanCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/dist'));
}); 




gulp.task('default', gulp.series('lint', 'minifyLibJs', 'minifyLibCss', () => {
  plugins.nodemon({
    debug: true,
    script: 'server.js',
    nodeArgs: ['--harmony', '--inspect=' + (config.server.debugPort || 5858)],
    ignore: ['public/**/*.*'],
//    watch: ['modules/*/server/**/*.*'],
    env: { 
      NODE_ENV: 'development' 
    }
  });
}));

gulp.task('prod', gulp.series('minifyLibJs', 'minifyAppJs', 'minifyLibCss', 'minifyAppCss', () => {
  plugins.nodemon({
    script: 'server.js',
    env: { 
      NODE_ENV: 'production' 
    }
  });
}));