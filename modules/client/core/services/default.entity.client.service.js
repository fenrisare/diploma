'use strict';

angular.module('core').service('DefaultEntityService', ['$resource', 'UuidService', 'Notificator', function ($resource, Uuid, Notificator) { 
  this.all = (resource) => {
    return (params, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.query(params, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };

  this.get = (resource) => {
    return (id, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.get({ id: id }, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };
  
  this.uniq = (resource) => {
    return (field, value, uuid, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.save({ id: 'uniq', field: field, value: value, uuid: uuid }, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };
  
  this.combobox = (resource) => {
    return (onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.query({ id: 'combobox' }, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };

  this.getWithParams = (resource) => {
    return (params, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.get(params, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };

  this.delete = (resource) => {
    return (object, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        return resource.delete({ id: object.uuid }, (data) => {
          return resolve(onSuccess ? onSuccess(data) : data);
        }, (error) => {
          reject(onError ? onError(error) : Notificator.showError(error));
        });
      });
    };
  };

  this.save = (resource, removeAttributesList) => {
    return (object, onSuccess, onError) => {
      return new Promise((resolve, reject) => {
        if(object.id) {
          return resource.update(object, (data) => {
            return resolve(onSuccess ? onSuccess(data) : data);
          }, (error) => {
            reject(onError ? onError(error) : Notificator.showError(error));
          });
        } else {
          return resource.save(object, (data) => {
            object.id = data.id;
            return resolve(onSuccess ? onSuccess(data) : data);
          }, (error) => {
            reject(onError ? onError(error) : Notificator.showError(error));
          });
        }
      });
    };
  };
  
  this.bindDefaultMethods = (target, resource) => {
    target.all = this.all(resource);
    target.get = this.get(resource);
    target.getWithParams = this.getWithParams(resource);
    target.delete = this.delete(resource);
    target.save = this.save(resource);
    target.uniq = this.uniq(resource);
    target.combobox = this.combobox(resource);
  };
}]);
