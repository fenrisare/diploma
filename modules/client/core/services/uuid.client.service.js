'use strict';

angular.module('core')
  .service('UuidService', [function () {
    /*
    * Generate new UUID
    */
    this.generateGuid = () => {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });
    };
    
    /*
    * Set UUID field for all inner entities
    */
    this.setGuid = (item, level) => {
      if(!item || level > 25 || typeof item !== 'object')
        return;
      else 
        level++;

      if(!item.itemGuid) {
        item.itemGuid = this.generateGuid();
      }
      _.each(item, (value, name) => {
        if (!String(name).startsWith("$") && value && typeof value !== "function") {
          if(Array.isArray(value)) {
            _.each(value, (arrayItem) => {
              this.setGuid(arrayItem, level);
            });
          } else if(typeof value === 'object') {
            this.setGuid(item[name], level);
          } 
        }
      });
    };
  }]);