'use strict';

angular.module('core').service('Locale', ['$translate', '$rootScope', '$mdDateLocale', function ($translate, $rootScope, $mdDateLocale) {
  this.getLanguages = () => {
    return RuntimeConfig.languages;
  };
  
  this.getLanguage = (code) => {
    return _.find(RuntimeConfig.languages, { code: code });
  };
  
  this.changeLocale = (code) => {
    let lang = this.getLanguage(code);
    $translate.use(lang.code).then(result => {
      let localeDate = moment.localeData(lang.short);
      $mdDateLocale.months = localeDate._months;
      $mdDateLocale.shortMonths = localeDate._monthsShort;
      $mdDateLocale.days = localeDate._weekdays;
      $mdDateLocale.shortDays = moment.localeData();
      $rootScope.$emit('$translateChanged');
    });
  };
}]);