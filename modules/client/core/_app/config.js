"use strict";

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
  // Init module configuration options
  let applicationModuleName = 'lasertag',
    apiUrl = '',
    languages = [{ code: 'rus', name: 'Русский' }],
    applicationModuleVendorDependencies = [
      'ngMaterial',
      'ngResource',
      'ngMessages',
      'ui.router',
      'ngSanitize',
      'md.data.table',
      'angular-bind-html-compile',
      'pascalprecht.translate',
      'ngMaterialDatePicker'
    ];

  // Parse and check runtime configuration
  // Make api URL
  try {
    if(RuntimeConfig) {
      if(RuntimeConfig.api) {
        apiUrl = apiUrl.concat(RuntimeConfig.api.protocol || window.location.protocol)
          .concat("//")
          .concat(RuntimeConfig.api.host || window.location.hostname)
          .concat(":")
          .concat(RuntimeConfig.api.port || window.location.port)
          .concat('/');
      }

      if(!RuntimeConfig.deviceRedirect) {
        RuntimeConfig.deviceRedirect = window.location.protocol + '//m-' + window.location.hostname + ':' + window.location.port + '/';
      }   
      if(!RuntimeConfig.brandName) {
        RuntimeConfig.brandName = 'SKELETON';
      }
      if(!RuntimeConfig.languages) {
        RuntimeConfig.languages = languages;
      }
    }
    
  } catch(err) {
    console.error('ACHTUNG! Can\'t read/load runtime config file. Check runtime.config file, and reload page. Error: ', err.message, ' \n\rStack: ', err.stack);
  }
    
  // Add a new vertical module
  var registerModule = function (moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || []);
    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    origin: apiUrl,
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule,
    languages: languages
  };
})();
