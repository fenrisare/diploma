'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular.module(ApplicationConfiguration.applicationModuleName).factory('UuidServiceInterceptor', ['UuidService', function (Uuid) {
  return {
    request: function (requset) {
      if (requset.method == 'POST' || requset.method == 'PUT') {
        if (!requset.data) return requset;
        Uuid.setGuid(requset.data, 0);
      }
      return requset;
    }
  };
}]);

angular.module(ApplicationConfiguration.applicationModuleName).factory('UuidServiceInterceptor', ['UuidService', function (Uuid) {
  return {
    request: function (requset) {
      if (requset.method == 'POST' || requset.method == 'PUT') {
        if (!requset.data) return requset;
        Uuid.setGuid(requset.data, 0);
      }
      return requset;
    }
  };
}]);


angular.module(ApplicationConfiguration.applicationModuleName).config(['$resourceProvider', '$locationProvider', '$httpProvider', '$mdDateLocaleProvider', '$translateProvider', function ($resourceProvider, $locationProvider, $httpProvider, $mdDateLocaleProvider, $translateProvider) {
    $locationProvider.html5Mode(true);
//    $sceProvider.enabled(false);    
    $httpProvider.interceptors.push('UuidServiceInterceptor');

    $translateProvider.useStaticFilesLoader({
      prefix: 'configuration/client/locale/locale-',
      suffix: '.json'
    });
  
    if(RuntimeConfig) {
      let lang = _.find(RuntimeConfig.languages || [], { code: RuntimeConfig.language});
      if(!lang) lang = { code: 'eng', short: 'en' };
      $translateProvider.preferredLanguage(lang.code);
      let localeDate = moment.localeData(lang.short);
//      $mdDateLocaleProvider.months = localeDate._months;
//      $mdDateLocaleProvider.shortMonths = localeDate._monthsShort;
//      $mdDateLocaleProvider.days = localeDate._weekdays;
//      $mdDateLocaleProvider.shortDays = moment.localeData();
    } else {
      $translateProvider.preferredLanguage('eng');
    }  

    $mdDateLocaleProvider.formatDate = function(date) {
      return date ? moment(date).format('DD.MM.YYYY') : null;
    };
  
    angular.extend($resourceProvider.defaults.actions, {
      query: {
        method: "GET",
        isArray: false
      },
      post: {
        method: 'POST'
      }
    });  
  }
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(['Auth', function (Auth) {
  Auth.loggedin();
}]);

//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
      window.history.pushState('', document.title, window.location.pathname);
    } else {
      // Prevent scrolling by storing the page's current scroll offset
      var scroll = {
        top: document.body.scrollTop,
        left: document.body.scrollLeft
      };
      window.location.hash = '';
      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scroll.top;
      document.body.scrollLeft = scroll.left;
    }
  }

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
