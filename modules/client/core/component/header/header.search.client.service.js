'use strict';

angular.module('core').service('HeaderSearchService', [ '$rootScope', '$transitions', function ($rootScope, $transitions) {         
  let self = this,
    data = {
      search: '',
      toggle: false
    };
  
  this.set = (search) => {
    data.search = search;
    return search;
  };
  
  this.reset = () => {
    data.search = '';
    data.toggle = false;
  };
  
  this.isShowing = () => {
    return data.toggle;
  };
  
  this.toggle = () => {
    data.toggle = !data.toggle;
  };
  
  this.get = () => {
    return data.search;
  };
  
  $transitions.onStart({ }, function(trans) {
    self.reset();
  });
}]);
