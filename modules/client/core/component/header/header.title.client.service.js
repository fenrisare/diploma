'use strict';

angular.module('core').service('HeaderTitleService', [ '$rootScope', '$transitions', function ($rootScope, $transitions) {         
  let self = this, 
    data = {
      title: ''
    };

  this.set = (title) => {
    data.title = title;
    return title;
  };
  
  this.isShowing = () => {
    return data.title ? true : false;
  };
  
  this.reset = () => {
    data.title = '';
  };
  
  this.get = (prefix) => {
    if(data.title)
      return (prefix || ' | ') + data.title;
  };
  
  $transitions.onStart({ }, function(trans) {
    self.reset();
  });
}]);
