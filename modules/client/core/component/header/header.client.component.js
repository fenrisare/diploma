"use strict";

angular.module('core').component('header', {
  templateUrl: 'modules/client/core/component/header/header.client.view.html',
  controller: ['$mdSidenav', 'HeaderTitleService', function($mdSidenav, HeaderTitle) {
    var $ctrl = this;
    $ctrl.getTitle = HeaderTitle.get;
    
    $ctrl.toggleLeft = () => {
      $mdSidenav('leftmenu').toggle();
    };
    
    $ctrl.brandName = RuntimeConfig.brandName;
  }]
});
