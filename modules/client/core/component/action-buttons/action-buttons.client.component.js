"use strict";

angular.module('core').component('actionButtons', {
  bindings: {
    save: '&',
    actionDisabled: '<'
  },
  templateUrl: 'modules/client/core/component/action-buttons/action-buttons.client.view.html',
  controler: [ function() {
    let $ctrl = this;
  }]
});