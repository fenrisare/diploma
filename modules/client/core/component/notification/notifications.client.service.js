'use strict';

angular.module('core').service('Notificator', [ '$rootScope','$state', '$mdToast', '$location', '$translate', function ($rootScope, $state, $mdToast, $location, $translate) {
  let self = this;
  
  let stripHtml = (object) => {
    if(object) {
      if(object.message) {
        return object.message.replace(/<(?:.|\n)*?>/gm, '');
      }else if(object.data) {
        return object.data.replace(/<(?:.|\n)*?>/gm, '');
      } else {
        return object.replace(/<(?:.|\n)*?>/gm, '');
      }
    }
    return object;
  };
  
  this.showError = (error) => {
    let toast = $mdToast.simple()
      .textContent(stripHtml(error.data || error))
      .theme('error-toast')  
//      .action('Детали')
      .highlightAction(true)
      .highlightClass('md-accent');

    $mdToast.show(toast).then(function(response) {
//      if ( response == 'ok' ) {
//        alert();
//      }
    });
  };

  this.notice = (title, message) => {
    Notification.primary({ 
      title: title,
      message: stripHtml(message)
    });
  };
  
  this.save = (toList) => {
    let toast = $mdToast.simple()
      .textContent($translate.instant('COMPONENT.NOTIFICATION.SAVE.TITLE'))
      .theme('success-toast')  
      .action($translate.instant('COMPONENT.NOTIFICATION.SAVE.TO_LIST'))
      .highlightAction(true)
      .highlightClass('md-accent');

    $mdToast.show(toast).then(function(response) {
      if ( response == 'ok' ) {
        if(toList) $state.go(toList);
      }
    });
  };
}]);


