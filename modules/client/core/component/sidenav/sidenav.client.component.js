"use strict";

angular.module('core').component('sideMenu', {
  templateUrl: 'modules/client/core/component/sidenav/sidenav.client.view.html',
  controller: ['Auth', '$mdSidenav', '$translate', function(Auth, $mdSidenav, $translate) {
    var $ctrl = this;
    $ctrl.Auth = Auth;
    
    $ctrl.logout = () => {
      return Auth.logout();
    };
    
    $ctrl.openMenu = function($mdMenu, ev) {
      $mdMenu.open(ev);
    };
    
    $ctrl.closeMenu = function() {
      $mdSidenav('leftmenu').close();
    };
    
    $ctrl.menu = [
      {
        title: $translate.instant('COMPONENT.SIDENAV.USER.TITLE'),
        description: $translate.instant('COMPONENT.SIDENAV.USER.DESCR'),
        icon: 'account-multiple',
        sref: 'user.list',
        permission: 'ADMIN.USER.VIEW'
      },
      {
        title: $translate.instant('COMPONENT.SIDENAV.ROLE.TITLE'),
        description: $translate.instant('COMPONENT.SIDENAV.ROLE.DESCR'),
        icon: 'account-key',
        sref: 'role.list',
        permission: 'ADMIN.ROLE.VIEW'
      },
      {
        title: $translate.instant('COMPONENT.SIDENAV.CUSTOMER.TITLE'),
        description: $translate.instant('COMPONENT.SIDENAV.CUSTOMER.DESCR'),
        icon: 'contact-mail',
        sref: 'customer.list',
        permission: 'ADMIN.CUSTOMER.VIEW'
      },
      {
        title: $translate.instant('COMPONENT.SIDENAV.LICENSE.TITLE'),
        description: $translate.instant('COMPONENT.SIDENAV.LICENSE.DESCR'),
        icon: 'file-document',
        sref: 'license.list',
        permission: 'ADMIN.LICENSE.VIEW'
      }
    ];
  }]
});