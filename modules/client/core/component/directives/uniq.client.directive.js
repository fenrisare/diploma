'use strict';

angular.module('core').directive('uniq', ['$q', function ($q) {
  return {
    require: "ngModel",
    scope: {
      uniqService: "=",
      uniqUuid: "=",
      uniq: "="
    },
    link: function (scope, element, attributes, ngModel) {
      if (!scope.uniq || !scope.uniqService) return;

      let uniqValidator = (modelValue) => {
        let deferred = $q.defer();
        scope.uniqService.uniq(scope.uniq, modelValue, scope.uniqUuid).then(result => {
          if (result.count) {
            deferred.reject();
          } else {
            delete ngModel.$asyncValidators.uniq;
            deferred.resolve();
          }
        });
        return deferred.promise;
      };

      element.on('focus', function () {
        if(!ngModel.$error.uniq){
          delete ngModel.$asyncValidators.uniq;
        }
      });

      element.on('blur', function () {
        ngModel.$asyncValidators.uniq = uniqValidator;
        ngModel.$validate();
      });
    }
  };
}]);
