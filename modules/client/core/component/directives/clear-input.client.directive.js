"use strict";
/*
 Angular Clear Input directive
 (c) 2014 Daniel Cohen. http://dcb.co.il
 License: MIT
 */
angular.module('core')
    .directive('clearInput', ['$parse',
        function($parse) {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, element, attr) {
                    var htmlMarkup = attr.clearBtnMarkup ? attr.clearBtnMarkup : '<md-icon class="mdi md-icon-button mdi-cancel" style="margin-left: -12px; position: absolute; right: 1px; left: calc(100% - 15px); top: 0;"></md-icon>';
                    var btn = angular.element(htmlMarkup);
                    element.after(btn);

                    btn.on('click', function(event) {
                        if (attr.clearInput) {
                            var fn = $parse(attr.clearInput);
                            scope.$apply(function() {
                                fn(scope, {
                                    $event: event
                                });
                            });
                        } else {
                            scope[attr.ngModel] = null;
                            scope.$digest();
                        }
                    });

                    scope.$watch(attr.ngModel, function(val) {
                        var hasValue = val && val.length > 0;
                        if (!attr.clearDisableVisibility) {
                            btn.css('visibility', hasValue ? 'visible' : 'hidden');
                        }

                        if (hasValue && !btn.hasClass('clear-visible')) {
                            btn.removeClass('clear-hidden').addClass('clear-visible');
                        } else if (!hasValue && !btn.hasClass('clear-hidden')) {
                            btn.removeClass('clear-visible').addClass('clear-hidden');
                        }
                    });
                }
            };
        }
    ]);