'use strict';

angular.module('core').directive('smartList', ['$parse', 'Auth', '$mdDialog', '$translate', function (parse, Auth, $mdDialog, $translate) {
  return {
    restrict: 'E',
    scope: {
      title: '<',
      state: '<',
      columns: '<',
      permissions: '<',
      options: '<',
      resource: '=',
      filter: '<',
      softFilter: '<'
    },
    templateUrl: 'modules/client/core/component/smart-list/smart-list.client.view.html',
    link: function(scope) {
      
      scope.idTitle = scope.idTitle ? scope.idTitle : 'ID';
      scope.firstLoaded = false;
      
      scope.options = scope.options || {};
      scope.options.newItemTitle = scope.options.newItemTitle || 'Создать';
      scope.options.createMode = scope.options.createMode !== undefined ? scope.options.createMode : true;
      scope.options.editMode = scope.options.editMode !== undefined ? scope.options.editMode : true;
      scope.options.deleteMode = scope.options.deleteMode !== undefined ? scope.options.deleteMode : true;   
      
      scope.Auth = Auth;
      scope.selected = [];
      scope.pageSizes = [15, 25, 50];
      scope.softFilter = scope.softFilter ? scope.softFilter : {};
      scope.params = {
        partialMode: true
      };
      scope.query = {
        pageSize: scope.pageSizes[0],
        page: 1
      };
      
      scope.hoverIn = (item) => {
        item.hovered = true;
      };
      scope.hoverOut = (item) => {
        item.hovered = false;
      };
      
      scope.getViewState = function(item, editMode) {
        var state = item.viewState || scope.state;
        return state + '.edit({id: "' + item.uuid + '", readOnly: ' + !editMode + '})';
      };
      
      scope.getTypedValue = function(item, column) {
        let value = parse(column.id)(item),
          type = column.type || (column.type = 'DEFAULT');
        switch(type.toLocaleLowerCase()) {
          case 'date':
            return value ? moment(value).format(column.format ? column.format : $translate.instant('GENERAL.FORMAT.DATETIME') || 'DD.MM.YYYY hh:mm:ss') : '';
          case 'boolean':
            if(value) {
              return '<md-icon style="color: green" class="mdi mdi-checkbox-marked-circle"></md-icon>';
            } else {
              return '<md-icon style="color: red" class="mdi mdi-close-circle"></md-icon>';
            }
            return value ? moment(value).format(column.format ? column.format : $translate.instant('GENERAL.FORMAT.DATETIME') || 'DD.MM.YYYY hh:mm:ss') : '';
          case 'link':
            if(column.source.indexOf('{id}') != -1) {
              column.source = column.source.replace('{id}', item.id);
            }
            return '<a href="' + column.source +'">' + value + '</a>';
          default:
            return value;
        }
      };
      
      scope.deleteItem = (item, index) => {
          let confirm = $mdDialog.confirm()
            .title($translate.instant('CONFIRMATION.DELETE.TITLE'))
            .textContent($translate.instant('CONFIRMATION.DELETE.DESCR'))
            .ok($translate.instant('CONFIRMATION.ANSWER.YES'))
            .cancel($translate.instant('CONFIRMATION.ANSWER.NO'));
       

        $mdDialog.show(confirm).then(function() {
          scope.resource.delete(item).then(result => {
            scope.loadItems();
          });
        });

      };
      
      scope.loadItems = function() {
        scope.promise = scope.resource.all({
          page: scope.query.page - 1,
          pageSize: scope.query.pageSize,
          filter: scope.filter,
          softFilter: scope.softFilter,
          softFilterStrictMatch: !scope.params.partialMode,
          order: scope.query.order
        }, function(result) {
          scope.result = result;
          scope.firstLoaded = true;
        });        
      };
      
      scope.$watch("currentPage", function(newValue) {
        // 1. Prevent first time multiple resource quering
        if(!scope.firstLoaded) return;
        if(!scope.resource) return;
        scope.loadItems();
      });

      scope.$watch('softFilter', function() {
        // 2. Prevent first time multiple resource quering
        if(!scope.firstLoaded) return;
        scope.loadItems();
        scope.currentPage = 0;
      }, true);
      
      // 3. Prevent first time multiple resource quering
      scope.loadItems();
    }
  };
}]);
