'use strict';

angular.module('core').directive('tableFilter', ['$parse', function ($parse) {
  return {
    restrict: 'E',
    scope: {
      type: '=',
      id: '=',
      filter: '=',
      source: '='
    },
    templateUrl: 'modules/client/core/component/smart-list/table-filter.client.view.html',
    link: function($scope) {
      if(!$scope.type || $scope.type == 'DEFAULT') { $scope.type = 'TEXT'; }
      $scope.loaded = true;
      $scope.localSearch = '';
      $scope.localDate = { startDate: '', endDate: '' };

      $scope.$watch('filter', function (filterValue) {
        if(!$scope.filter) {
          return;
        }
        if (!$scope.filter[$scope.id])
          delete $scope.filter[$scope.id];
      }, true);

      $scope.changeTextFilter = function(value) {
        if(value) {
          $scope.filter[$scope.id] = {
            value: value,
            type: 'TEXT'
          };
        } else {
          delete $scope.filter[$scope.id];
        }
      };

      $scope.changeDateFilter = function(value) {
        if(value.startDate ||
           value.endDate) {
          $scope.filter[$scope.id] = {
            startDate: value.startDate ? value.startDate.toDateString() : null,
            endDate: value.endDate ? value.endDate.toDateString() : null,
            type: $scope.type
          };
        } else {
          delete $scope.filter[$scope.id];
        }
      };
    }
  };
}]);
