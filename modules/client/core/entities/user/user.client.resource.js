'use strict';

angular.module('core').service('UserResource', ['$resource', function($resource) {
  return $resource(ApplicationConfiguration.origin + 'api/user/:id', {
    id: '@id'
  }, {
    update: {
      method: 'PUT'
    }
  });
}]);
