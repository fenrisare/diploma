'use strict';

angular.module('core').service('UserService', ['DefaultEntityService', 'UserResource', function(DefaultEntity, User) {
  DefaultEntity.bindDefaultMethods(this, User);
}]);