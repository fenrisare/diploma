'use strict';

angular.module('core').service('UserConfig', ['$translate', function($translate) {
  this.columns = [
    {
      id: 'isActive',
      locale: '',
      type: 'BOOLEAN'
    },
    {
      id: 'login',
      locale: $translate.instant('ENTITY.USER.LIST.COLUMNS.LOGIN')
    },
    {
      id: 'name',
      locale: $translate.instant('ENTITY.USER.LIST.COLUMNS.NAME')
    },
    {
      id: 'email',
      locale: $translate.instant('ENTITY.USER.LIST.COLUMNS.EMAIL')
    },
    {
      id: 'registerDate',
      locale: $translate.instant('ENTITY.USER.LIST.COLUMNS.REGISTER'),
      type: 'DATE',
      format: $translate.instant('GENERAL.FORMAT.DATE'),
      classes: 'hidden-xs hidden-sm'
    },
    {
      id: 'roleName',
      locale: $translate.instant('ENTITY.USER.LIST.COLUMNS.ROLE'),
//      classes: 'hidden-xs hidden-sm'
    }
  ];
}]);