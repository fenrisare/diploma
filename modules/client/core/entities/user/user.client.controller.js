"use strict";

angular.module('core').controller('UserListController', ['$scope', 'UserService', 'UserConfig', 'HeaderTitleService', '$translate', function($scope, User, Config, HeaderTitle, $translate) {
  HeaderTitle.set($translate.instant('ENTITY.USER.TITLE.LIST'));
  $scope.resource = User;
  $scope.state = 'user';
  $scope.permissions = {
    edit: 'ADMIN.USER.EDIT',
    view: 'ADMIN.USER.VIEW'
  };
  $scope.columns = Config.columns;
}]).controller('UserEditController', ['$scope', 'UserService', 'UserConfig', 'HeaderTitleService', '$stateParams', 'user', 'roles', '$state', 'Notificator', '$translate', function($scope, User, Config, HeaderTitle, $stateParams, user, roles, $state, Notificator, $translate) {
  $scope.isNew = $stateParams.id ? false : true;
  $scope.User = User;
  
  if($scope.isNew) {
    HeaderTitle.set($translate.instant('ENTITY.ROLE.TITLE.NEW'));
    $scope.user = {};
  } else {
    HeaderTitle.set($translate.instant('ENTITY.ROLE.TITLE.EDIT'));
    $scope.user = user;
  }
  $scope.roles = roles;
  
  $scope.save = () => {
    User.save($scope.user).then(result => {
      Notificator.save('user.list');
      $state.go('user.edit', { id: result.uuid });
    });
  };
}]);

      