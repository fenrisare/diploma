'use strict';

// Setting up route
angular.module('core').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider
      .state('user', {
        url: '/user',
        abstract: true
      })
      .state('user.list', {
        url: '',
        controller: 'UserListController',
        templateUrl: 'modules/client/core/component/list-card/list-card.client.view.html'
      })
      .state('user.edit', {
        url: '/:id',
        controller: 'UserEditController',
        templateUrl: 'modules/client/core/entities/user/user-edit.client.view.html',
        resolve: {
          user: ['$stateParams', 'UserService', function($stateParams, User) {
            if($stateParams.id) return User.get($stateParams.id);
            else return {};
          }],
          roles: ['RoleService', function(Role) {
            return Role.combobox();
          }]
        }
      });
  }
]);