'use strict';

angular.module('core').service('RoleConfig', ['$translate', function($translate) {
  this.columns = [
    {
      id: 'name',
      locale: $translate.instant('ENTITY.ROLE.LIST.COLUMNS.NAME'),
    },
    {
      id: 'usersCount',
      locale: $translate.instant('ENTITY.ROLE.LIST.COLUMNS.USERS_COUNT')
    }
  ];
}]);