'use strict';

// Setting up route
angular.module('core').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider
      .state('role', {
        url: '/role',
        abstract: true
      })
      .state('role.list', {
        url: '',
        controller: 'RoleListController',
        templateUrl: 'modules/client/core/component/list-card/list-card.client.view.html'
      })
      .state('role.edit', {
        url: '/:id',
        controller: 'RoleEditController',
        templateUrl: 'modules/client/core/entities/role/role-edit.client.view.html',
        resolve: {
          role: ['$stateParams', 'RoleService', function($stateParams, Role) {
            if($stateParams.id) return Role.get($stateParams.id);
            else return {};
          }],
          permissions: ['PermissionService', function(Permission) {
            return Permission.all();
          }]
        }
      });
  }
]);