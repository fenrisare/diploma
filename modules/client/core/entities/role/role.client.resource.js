'use strict';

angular.module('core').service('RoleResource', ['$resource', function($resource) {
  return $resource(ApplicationConfiguration.origin + 'api/role/:id', {
    id: '@id'
  }, {
    update: {
      method: 'PUT'
    }
  });
}]);
