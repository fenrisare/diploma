"use strict";

angular.module('core').controller('RoleListController', ['$scope', 'RoleService', 'RoleConfig', 'HeaderTitleService', '$translate', function($scope, Role, Config, HeaderTitle, $translate) {
  HeaderTitle.set($translate.instant('ENTITY.ROLE.TITLE.LIST'));
  $scope.resource = Role;
  $scope.state = 'role';
  $scope.permission = {
    edit: 'ADMIN.ROLE.EDIT',
    view: 'ADMIN.ROLE.VIEW'
  };
  $scope.columns = Config.columns;
}])
  .controller('RoleEditController', ['$scope', 'RoleService', 'RoleConfig', 'HeaderTitleService', '$stateParams', 'role', 'permissions', '$state', 'Notificator', '$translate', function($scope, Role, Config, HeaderTitle, $stateParams, role, permissions, $state, Notificator, $translate) {
  $scope.isNew = $stateParams.id ? false : true;
  $scope.Role = Role;
  
  if($scope.isNew) {
    HeaderTitle.set($translate.instant('ENTITY.ROLE.TITLE.NEW'));
    $scope.role = {};
  } else {
    HeaderTitle.set($translate.instant('ENTITY.ROLE.TITLE.EDIT'));
    $scope.role = role;
  }
    $scope.permissions = permissions;
  
  $scope.save = () => {
    Role.save($scope.role).then(result => {
      Notificator.save('role.list');
      $state.go('role.edit', { id: result.uuid });
    });
  };
}])
;

      