'use strict';

angular.module('core').service('RoleService', ['DefaultEntityService', 'RoleResource', function(DefaultEntity, Role) {
  DefaultEntity.bindDefaultMethods(this, Role);
}]);