'use strict';

angular.module('core').service('PermissionService', ['DefaultEntityService', 'PermissionResource', function(DefaultEntity, Permission) {
  DefaultEntity.bindDefaultMethods(this, Permission);
}]);