'use strict';

angular.module('core').service('PermissionResource', ['$resource', function($resource) {
  return $resource(ApplicationConfiguration.origin + 'api/permission/:id', {
    id: '@id'
  }, {
    update: {
      method: 'PUT'
    }
  });
}]);
