'use strict';


angular.module('core').service('AuthResource', ['$resource', function($resource) {
  return {
    login: (credentials, success, error) => {
      return $resource(ApplicationConfiguration.origin + 'api/auth/login').post(credentials, success, error);
    },
    logout: (success) => {
      return $resource(ApplicationConfiguration.origin + 'api/auth/logout').post(null, success);
    },
    register: (user, success, error) => {
      $resource(ApplicationConfiguration.origin + 'api/auth/register').post(user, success, error);
    },
    loggedin: (success, error) => {
      return $resource(ApplicationConfiguration.origin + 'api/auth/loggedin').post({}, success);
    } 
  };
}]);

angular.module('core').factory('Auth', ['Notificator', 'AuthResource', '$rootScope', '$transitions', '$state', function(Notificator, AuthResource, $rootScope, $transitions, $state) {
  let setUser = (user) => {
    auth.user = user || null;
  };
  
  let getUser = () => {
    return auth.user;
  };
    
  let isUnregistred = () => {
    return !getUser();
  };
  
  let getPermission = (permission) => {
    if(isUnregistred()) return false;
    if(!permission) return true;
    if(!getUser().permissions) return false;
    return getUser().permissions[permission];
  };
    
  let isAdmin = () => {
    return getPermission('FULL_ADMIN');
  };
  
  let cPerm = (permission) => {
    if(isAdmin()) return true;
    return getPermission(permission);
  };

  
  let login = (credentials, success, error) => {
    return new Promise((resovle, reject) => {
      if(!credentials.isHashed) {
        credentials.password = sha256(credentials.password);
        credentials.isHashed = true;
      }
      let result = AuthResource.login(credentials, result => {
        setUser(result);
        resovle(success ? success(result) : result);
      }, err => {
        reject(Notificator.showError(err));
      });
    });
  };
  
  let logout = () => {
    AuthResource.logout(result => {
      setUser();
    });
  };
  
  let register = (user) => {
    return new Promise((resovle, reject) => {
      if(!user.isHashed) {
        user.password = sha256(user.password);
        user.isHashed = true;
      }
      AuthResource.register(user, result => {
        resovle(result);
      }, err => {
        reject(Notificator.showError(err));
      });
    });
  };
  
  let loggedin = () => {
    AuthResource.loggedin(user => {
      if(user.id) {
        setUser(user);
      } else {
        setUser();
      }
    });
  };
  
  //Listeners
  $rootScope.$on('LOGOUT', event => {
    if(auth.user) logout();
  });
  
  $transitions.onStart( { to: '*' }, function(trans) {
    loggedin();
  });
  
  var auth = {
    user: null,
    loggedin: loggedin,
    register: register,
    logout: logout,
    login: login,
    cPerm: cPerm,
    isAdmin: isAdmin,
    isUnregistred: isUnregistred
  };
  return auth;
  
}]);
