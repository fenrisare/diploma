"use strict";

angular.module('core').controller('AuthController', ['$scope', '$state', 'Auth', 'UserService', function($scope, $state, Auth, User) {
  $scope.newUser = {};
  $scope.validation = {};
  $scope.User = User;
  
  $scope.loginAction = (login, password) => {
    if(!login || !password) return;
    Auth.login({ login: login, password: password}).then(result => {
      $state.go('main');
    });
  };
  
  $scope.signupAction = () => {
    Auth.register(angular.copy($scope.newUser)).then((result) => {
      $scope.loginAction($scope.newUser.login, $scope.newUser.password);
    });
  };
}]);