'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', function($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      controller: 'AuthController',
      templateUrl: 'modules/client/core/entities/auth/login.client.view.html'
    })
    .state('logout', {
      url: '/logout',
      controller: ['Auth', function(Auth) {
        Auth.logout();
      }]
    });
  }
]);