'use strict';

let utils  = require(LOCALVARS.utilsPath),
    _  = require('lodash');

module.exports = (app, db, asset) => {
  app.use((err, req, res, next) => {
    if (!err) return next();
    console.error(err.stack);
    res.redirect('/server-error');
  });
  
  app.get('/api/', function(req, res) {
    res.sendfile('public/api-help.html');
  });

  app.get('/api/alive', function(req, res) {
    res.send({ alive: true });
  });
};
