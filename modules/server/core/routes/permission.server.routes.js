'use strict';

var Service = require('../service/permission.server.service').Service,
  middleware = require(global.LOCALVARS.middleware);

module.exports = function(app, db) {
  var Api = new Service(db);

  app.route('/api/permission')
    .get(middleware.checkUserPermissions(['ADMIN.PERMISSION.VIEW']), Api.all())
    .post(middleware.checkUserPermissions(['ADMIN.PERMISSION.VIEW']), Api.save());
  app.route('/api/permission/uniq')
    .post(Api.uniq());
  app.route('/api/permission/hierarchy')
    .get(middleware.checkUserPermissions(['ADMIN.PERMISSION.VIEW']), Api.allHierarchy());
  app.route('/api/permission/combobox')
    .get(Api.all({ scope: 'combobox' }));
  app.route('/api/permission/:id')
    .get(middleware.checkUserPermissions(['ADMIN.PERMISSION.VIEW']), Api.get())
    .put(middleware.checkUserPermissions(['ADMIN.PERMISSION.EDIT']), Api.save())
    .delete(middleware.checkUserPermissions(['ADMIN.PERMISSION.EDIT']), Api.delete());
};
