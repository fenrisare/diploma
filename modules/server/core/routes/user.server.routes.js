'use strict';

var Service = require('../service/user.server.service').Service,
  middleware = require(global.LOCALVARS.middleware);

module.exports = function(app, db) {
  var Api = new Service(db);

  app.route('/api/user')
    .get(middleware.checkUserPermissions(['ADMIN.USER.VIEW']), Api.all())
    .post(Api.save());
  app.route('/api/user/combobox')
    .get(Api.all({ scope: 'combobox' }));
  app.route('/api/user/:id')
    .get(middleware.selfGetUser(['ADMIN.USER.VIEW']), Api.get())
    .put(middleware.checkUserPermissions(['ADMIN.USER.EDIT']), Api.save())
    .delete(middleware.checkUserPermissions(['ADMIN.USER.EDIT']), Api.delete());
  app.route('/api/user/uniq')
    .post(Api.uniq());
};
