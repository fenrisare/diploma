'use strict';

var Service = require('../service/role.server.service').Service,
  middleware = require(global.LOCALVARS.middleware);

module.exports = function(app, db) {
  var Api = new Service(db);

  app.route('/api/role')
    .get(middleware.checkUserPermissions(['ADMIN.ROLE.VIEW']), Api.all())
    .post(middleware.checkUserPermissions(['ADMIN.ROLE.EDIT']), Api.save());
  app.route('/api/role/combobox')
    .get(Api.all({ scope: 'combobox' }));
 app.route('/api/role/:id')
   .get(middleware.checkUserPermissions(['ADMIN.ROLE.VIEW']), Api.get())
   .put(middleware.checkUserPermissions(['ADMIN.ROLE.EDIT']), Api.save())
   .delete(middleware.checkUserPermissions(['ADMIN.ROLE.EDIT']), Api.delete());
  app.route('/api/role/uniq')
    .post(Api.uniq());
};
