'use strict';

let authService = require('../service/auth.server.service.js'),
    utils  = require(LOCALVARS.utilsPath),
    _  = require('lodash');

module.exports = (app, db, asset) => {
  app.post('/api/auth/login', function(req, res) {
    return authService.login(req, res);
  });
  app.post('/api/auth/logout', function(req, res) {
    return authService.logout(req, res);
  });
  app.post('/api/auth/register', function(req, res) {
    return authService.register(req, res);
  });
  app.post('/api/auth/loggedin', function(req, res) {
    return authService.loggedin(req, res);
  });
};
