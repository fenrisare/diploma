'use strict';

let session = require('express-session'),
    SQLiteStore = require('connect-sqlite3')(session);

module.exports = {
  init: (app, asset) => {
    app.use(session({
      secret: asset.server.sessionSecret,
      resave: false,
      saveUninitialized: true,
      store: new SQLiteStore({ db: 'sessions.db', dir: './sessions' }),
      cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 },
    }));
  }
};