'use strict';

let swig  = require('swig'),
  path = require('path'),
  utils = require(LOCALVARS.utilsPath),
  conf = require(LOCALVARS.mainCfg),
  _ = require('lodash');

module.exports = {
  init: (app, db, asset) => {
    _.each(utils.getGlobbedPaths(asset.server.routes) || [], (route) => {
      require(path.resolve(route))(app, db, asset);
    });
  },
  initMainRoute: (app, asset) => {
    // RENDER INDEX HTML
    app.get('*', function(req, res) {

      let sources = {
        title: conf.title || 'SKELETON'
      };
      if(process.env.NODE_ENV != 'development') {
        sources.jsFiles = _.union(asset.client.lib.js, asset.client.app.js);
        sources.cssFiles = _.union(asset.client.lib.css, asset.client.app.css);
      } else {
        sources.jsFiles = utils.getGlobbedPaths(_.union(asset.client.lib.js, asset.client.app.js));
        sources.cssFiles = utils.getGlobbedPaths(_.union(asset.client.lib.css, asset.client.app.css));
      }
      res.send(swig.renderFile('modules/server/core/index.html', sources));
    });

  }
};