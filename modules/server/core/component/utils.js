'use strict';

let glob = require('glob'),
  path = require('path'),
  _ = require('lodash'),
  errors = require('./errors');

LOCALVARS.utilsPath = __filename;

module.exports = {
  ERR: errors,
  path: path,
  getGlobbedPaths: (globPatterns, excludes) => {
    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    // The output array
    var output = [];

    // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
      globPatterns.forEach(function (globPattern) {
        output = _.union(output, module.exports.getGlobbedPaths(globPattern, excludes));
      });
    } else if (_.isString(globPatterns)) {
      if (urlRegex.test(globPatterns)) {
        output.push(globPatterns);
      } else {
        var files = glob.sync(globPatterns);
        if (excludes) {
          files = files.map(function (file) {
            if (_.isArray(excludes)) {
              for (var i in excludes) {
                file = file.replace(excludes[i], '');
              }
            } else {
              file = file.replace(excludes, '');
            }
            return file;
          });
        }
        output = _.union(output, files);
      }
    }
    return output;
  },
  hashPassword: (pwd) => {
    var hash = require('crypto').createHash('sha256');
    hash.update(pwd);
    return hash.digest('hex');
  },
  handleError: (code, err, res) => {
    var error = {
      code: code,
    };
    if(err.message) {
      error.message = err.message;
    } else {
      error.message = err;
    }

    if(err.trace) {
      error.trace = err.trace;
    }
    
    if(res) {
      res.status(code).send(error);
    } else {
      return error;
    }
  },
  parse: (layer, path, value) => {
    var i = 0;
    path = path.split('.');

    for (; i < path.length; i++)
      if (value != null && i + 1 === path.length)
        layer[path[i]] = value;
    layer = layer[path[i]];

    return layer;
  }
};
