'use strict';

let Sequelize = require('sequelize'),
  utils = require(LOCALVARS.utilsPath),
  _ = require('lodash'),
  dbProperties = require(utils.path.resolve('configuration/server/db/local.db.json')),
  BPrimose = require("bluebird"),
  Postgrator = require('postgrator');



module.exports = {
  db: null,
  initDb: (app, asset) => {
    module.exports.db = new Sequelize(dbProperties.db, dbProperties.user, dbProperties.password, {
      host: dbProperties.host,
      dialect: dbProperties.dialect,
      pool: dbProperties.pool,
      operatorsAliases: false
    });
  
    let postgrator = new Postgrator({
      migrationDirectory: 'migrations',
      driver: 'pg',
      host: dbProperties.host,
      port: dbProperties.port || 5432,
      database: dbProperties.db,
      username: dbProperties.user,
      password: dbProperties.password,
      schemaTable: 'schemaversion'
    });
    
    console.log('--------------- CHECK FOR NEW MIGRATIONS ---------------');
    return postgrator.migrate().then(appliedMigrations => { // LOG MIGRATION
      if(!appliedMigrations || appliedMigrations.length === 0) {
        console.log('---------------- NO MIGRATIONS TO APPLY ----------------');
      } else {
        console.log('------------------ SUCCESSFULL MIGRATE -----------------');
        _.each(appliedMigrations, (migration) => {
          console.log('Successfully applied migration ' + migration.filename);
        });
        console.log('--------------------------------------------------------');
      }
      return true;
    }).then(res => { // INIT MODELS AFTER MIGRATION
      return module.exports.initModels(module.exports.db, asset);
    }).catch(error => {
      console.log('-------------------- MIGRATION ERROR -------------------');
      console.log(error);
      console.log('--------------------------------------------------------');
      return error;
    });
    
  },
  initModels: (db, asset) => {
    return new BPrimose((resolve, reject) => {
      // Import sequelize models to db.models
        let Models = [];
      _.each(utils.getGlobbedPaths(asset.server.models) || [], (modelPath) => {
        console.info('Importing model from ', modelPath);
        Models.push(db.import(utils.path.resolve(modelPath)));
      });
      // Sync models with DB tables
      return db.sync().then(() => {
        // Customize models
        return BPrimose.each(Models, model => {
          // Build associations
          if ('associate' in model) model.associate(db.models);
          if ('loadScopes' in model) model.loadScopes(db.models);

          // If it's materialized view - refresh it
          if('refreshMaterializedView' in model) {
            model.refreshMaterializedView().catch(err => {
              console.log(err);
            });
          }
          return null;
        }).then(() => {
          return resolve(db);
        });
      }).catch(err => {
        console.error(err);
        reject(err);
      });
    });
  }
};
