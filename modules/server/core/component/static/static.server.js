'use strict';

let path = require('path'),
  utils = require(LOCALVARS.utilsPath),
  _ = require('lodash');

module.exports = {
  init: (app, express) => {
    app.use('/', express.static(path.resolve('./public')));
    app.use('/node_modules', express.static(path.resolve('node_modules')));
    app.use('/configuration/client', express.static(path.resolve('configuration/client')));
    _.each(utils.getGlobbedPaths('modules/client/*') || [], folderPath => {
      app.use('/' + folderPath, express.static(folderPath));
    });
  }
};