'use strict';

let glob = require('glob'),
  path = require('path'),
  _ = require('lodash');

if(global.LOCALVARS) {
  global.LOCALVARS.middleware = __filename;
} else {
  global.LOCALVARS = {
    middleware: __filename
  };
}

module.exports = {
  selfGetUser: (permissions, logic) => {
    return (req, res, next) => {
      if(req.session.user && req.session.user.uuid == req.params.id) {
        return next();
      } else {
        return module.exports.checkUserPermissions(permissions, logic)(req, res, next);
      }
    };
  },
  checkUserPermissions: (permissions, logic, defaultReturn) => {
    return (req, res, next) => {
      let checkResult = false;
      if(req.headers.access && req.headers.access == '431198ad-fb01-4ab6-9779-3ea5fdf7deb4') {
        return next();
      }
      if(!permissions) {
        return next();
      }
      if(!req.session.user) {
        return defaultReturn ? res.send(defaultReturn) : res.status(403).send(defaultReturn);
      }
      if(!logic) {
        logic = 'and';
      }
      if(_.indexOf(req.session.user.permissions, 'FULL_ADMIN') != -1 || req.session.user.permissions.FULL_ADMIN) {
        return next();
      }
      
      if(logic.toLowerCase() == 'or') {
        _.each(permissions, (requstedPermission) => {
          if(req.session.user.permissions[requstedPermission]) {
            checkResult = true;
          }
        });
      } else {
        checkResult = true;
        _.each(permissions, (requstedPermission) => {
          if(!req.session.user.permissions[requstedPermission]) {
            checkResult = false;
          }
        });
      }
      
      if(!checkResult) {
        return defaultReturn ? res.send(defaultReturn) : res.status(403).send(defaultReturn);
      } else {
        return next();
      }
    };
  }
};
