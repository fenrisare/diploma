'use strict';

module.exports = {
  USER: {
    NOT_UNIQ_LOGIN: {
      status: 400,
      name: 'Bad Request',
      message: 'Пользователь с таким логином уже существует'
    },
    NOT_UNIQ_EMAIL: {
      status: 400,
      name: 'Bad Request',
      message: 'Пользователь с таким email\'ом уже существует'
    }
  }
};
