'use strict';

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('permissionMv', {
    id: { type: DataTypes.INTEGER, primaryKey: true },
    code: DataTypes.UUID,
    description: DataTypes.STRING,
    uuid: DataTypes.UUID,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    timestamps: false,
    freezeTableName: true,
    defaultScope: {
      where: {
        deletedAt: null
      }
    },
    scopes: {
      combobox: {
        attributes: ['id', 'code']
      }
    }
  });
  
  model.refreshMaterializedView = () => {
    return sequelize.query('REFRESH MATERIALIZED VIEW public."' + model.name + '";');
  };
  
  return model;
};
