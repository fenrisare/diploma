'use strict';

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('userMv', {
    id: { type: DataTypes.INTEGER, primaryKey: true },
    login: DataTypes.STRING,
    name: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    email: DataTypes.STRING,
    registerDate: DataTypes.DATE,
    uuid: DataTypes.UUID,
    roleName: DataTypes.STRING
  }, {
    timestamps: false,
    freezeTableName: true,
    defaultScope: {
      where: {
        deletedAt: null
      }
    },
    scopes: {
      combobox: {
        attributes: ['id', 'name', 'login']
      }
    }
  });  

  return model;
};
