'use strict';

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('roleMv', {
    id: DataTypes.STRING,
    name: DataTypes.STRING,
    usersCount: DataTypes.INTEGER,
    uuid: { type: DataTypes.INTEGER, primaryKey: true },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    timestamps: false,
    freezeTableName: true,
    defaultScope: {
      where: {
        deletedAt: null
      }
    },
    scopes: {
      combobox: {
        attributes: ['id', 'name']
      }
    }
  });

  return model;
};
