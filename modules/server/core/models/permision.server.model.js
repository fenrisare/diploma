'use strict';

let hook = (models) => {
  return (instance, options) => {
    models.permissionMv.refreshMaterializedView();
  };
};

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('permission', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    hash: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    timestamps: true,
    freezeTableName: true,
    hooks: {
      afterCreate: hook(sequelize.models),
      afterDestroy: hook(sequelize.models),
      afterBulkDestroy: hook(sequelize.models),
      afterUpdate: hook(sequelize.models),
      afterSave: hook(sequelize.models),
      afterUpsert: hook(sequelize.models)
    }
  });

  model.associate = (models) => {
    model.belongsToMany(models.role, {through: 'RolePermission', foreignKey: 'permissionId'});
  };
  
  model.loadScopes = (models) => {
    model.addScope('defaultScope', {
      where: {
        deletedAt: null
      }
    }, { 
      override: true 
    });
    model.addScope('combobox', {
      attributes: ['id', 'code']
    });
  };
  return model;
};
