'use strict';

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('role', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    hash: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    timestamps: true,
    freezeTableName: true
  });

  model.associate = (models) => {
    model.belongsToMany(models.permission, {through: 'RolePermission', foreignKey: 'roleId'});
  };
  
  model.loadScopes = (models) => {
    model.addScope('defaultScope', {
      include: [
        { 
          model: sequelize.models.permission,
          required: true
        }
      ],
      where: {
        deletedAt: null
      }
    }, { 
      override: true 
    });
    model.addScope('combobox', {
      attributes: ['id', 'name']
    });
  };
  
  return model;
};
