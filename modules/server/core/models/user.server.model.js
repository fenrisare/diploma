'use strict';

let utils = require(LOCALVARS.utilsPath),
  UserService = require('../service/user.server.service').Service;

module.exports = function(sequelize, DataTypes) {
  let model = sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    roleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    login: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [3, 60],
          msg: 'Длинна логина должна быть от 3 до 60 символов'
        },
        is: {
          args: [/^[A-Za-z0-9-_]*$/],
          msg: 'Логин может содержать только латиницу и цифры'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: {
          args: [0, 100],
          msg: 'Длинна имени не должна превышать 100 символов'
        },
        is: {
          args: [/^[A-Za-z0-9-_А-Яа-яЁёІіЇїєЄ' ]*$/],
          msg: 'Имя может содержать буквы и цифры и символы -_'
        }
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: {
          args: [0, 100],
          msg: 'Длинна email\'a не должна превышать 100 символов'
        },
        isEmail: {
          msg: 'Неверный формат Emeil\'a'
        }
      }
    },
    registerDate: {
      type: DataTypes.DATE,
      defaultValue: new Date()
    },
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    hash: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    activationId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    timestamps: true,
    freezeTableName: true,
    validate: {
      isUniqLogin () {
        return new UserService(sequelize).iUniq('login', this.getDataValue('login'), this.getDataValue('uuid'), this.getDataValue('transaction')).then(result => {
          if(result)
            throw new Error('Пользователь с таким логином уже существует');
          else
            return true;
        });
      },
      isUniqEmail ()  {
        // Skip because email is not required
        if(!this.getDataValue('email')) return true;
        return new UserService(sequelize).iUniq('email', this.getDataValue('email'), this.getDataValue('uuid'), this.getDataValue('transaction')).then(result => {
          if(result)
            throw new Error('Пользователь с таким email\'om уже существует');
          else
            return true;
        });
      }
    },
  });
  
  model.associate = (models) => {
    model.belongsTo(models.role);
  };

  model.loadScopes = (models) => {
    model.addScope('defaultScope', {
      include: [
        { 
          model: sequelize.models.role
        }
      ],
      where: {
        deletedAt: null
      }
    }, { 
      override: true 
    });
    model.addScope('combobox', {
      attributes: ['id', 'name', 'login']
    });
  };
  
  return model;
};
