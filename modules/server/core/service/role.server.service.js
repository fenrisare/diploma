'use strict';

var DefaultService = require('./default.server.service').DefaultService,
  utils = require(LOCALVARS.utilsPath),
  _ = require('lodash');

class Service extends DefaultService {
  constructor(db) {
    return module.exports.service ? module.exports.service : module.exports.service = super(db.models.role, db.models.roleMv, db);
  }
  get() {
    return (req, res) => {
      let service = this;
      service.iGet({
        softFilter: {
          uuid: { value: req.params.id, logic: 'eq' }
        }
      }).then(role => {
        let permissions = {};
        _.each(role.getDataValue('permissions'), rp => {
          permissions[rp.getDataValue('code')] = true;
        });
        role.setDataValue('permissions', permissions);
        return role;
      }).then(result => {
        res.send(result);
      });
    };
  }
  save() {
    return (req, res) => {
      let role = req.body,
        service = this,
        permissions = req.body.permissions;

      return service.iTransactional((transaction) => {
        return service.iSave(role).then(result => {
          let codes = _.map(permissions, (pr, name) => {
            return name;
          });

          return service.db.models.permission.findAll({
            where: {
              code: codes
            }
          }).then(permissions => {
            return result.setPermissions(permissions, { transaction: transaction ? transaction : null });
          }).then( () => {
            res.send({ uuid: role.uuid });
          });
        });
      }).catch(err => {
          return utils.handleError(500, err, res);
      });
    };
  }
}
exports.Service = Service;
