'use strict';

let _ = require('lodash'),
  uuid = require('uuid/v4'),
  coreUtils = require(LOCALVARS.utilsPath),
  moment = require('moment');

class DefaultServiceUtils {  // Default class utils
  constructor() {
    this.successStatus = 200;
    this.errStatus = 500;
  }

  generateUuid() {
    return uuid();
  }
  
  handleError(res, err) {
    let outputErr = {
      status: err.status || this.errStatus,
      statusText: err.name,
      data: err.message
    };
    console.log(err.stack);
    return res.status(err.status || this.errStatus).send(outputErr);
  }
}

class DefaultService {
  constructor(Model, ViewModel, db) {
    this.Model = Model;
    this.ViewModel = ViewModel;
    this.db = db;
    this.serviceUtils = new DefaultServiceUtils();
  }

  all(params) {
    let service = this;
    return (req, res) => {
      let payload = service.buildPayload(params, req, 'all');
      return service.iAll(payload).then((result) => {
        return res.send(result);
      }).catch((err) => {
        return service.serviceUtils.handleError(res, err);
      });
    };
  }

  iAll(payload) {
    let service = this;
    payload = service.processPayload(payload, service.ViewModel);
    return service.ViewModel.scope(payload.scope).findAndCountAll(payload).then((result) => {
      return result;
    });
  }

  get(params) {
    let service = this;
    return (req, res) => {
      let payload = service.buildPayload(params, req, 'get');
      return service.iGet(payload).then((result) => {
        return res.send(result);
      }).catch((err) => {
        return service.serviceUtils.handleError(res, err);
      });
    };
  }

  iGet(payload) {
    let service = this;
    payload = service.processPayload(payload, service.Model);
    return service.Model.scope(payload.scope).findOne(payload).then((result) => {
      return result;
    });
  }

  save(params) {
    let service = this;
    return (req, res) => {
      return service.iSave(req.body).then((result) => {
        return res.send({ uuid: result.uuid });
      }).catch((err) => {
        return service.serviceUtils.handleError(res, err);
      });
    };
  }

  // TODO: save files
  iSave(item, files, transaction) {
    let service = this;
    if (Array.isArray(item)) {
      // TODO: save files
      return this.db.Promise.map(item, (it) => {
        return service.iSave(it, null, transaction);
      }).then(result => {
        return result;
      });
    } else {
      return service.checkHash(item, transaction).then(result => {
        item = service.clearModelItem(item);
        item.hash = service.serviceUtils.generateUuid();
        if(!item.uuid) {
          item.uuid = service.serviceUtils.generateUuid();
        }
        if(!item.id) {
          return service.Model.create(
            item,
            { transaction: transaction ? transaction : null }
          ).then((result) => {
            return result;
          });
        } else {
          return service.Model.update(
            item,
            {
              where: item.id ? { id: item.id } : { uuid: item.uuid },
              transaction: transaction ? transaction : null,
              returning: true,
            }
          ).then((result) => {
            return result[1][0];
          });
        }
      });
    }
  }

  delete(transaction) {
    let service = this;
    return (req, res) => {
      let uuid = req.params.uuid || req.params.id;
      return service.iDelete(uuid, transaction).then((result) => {
        return res.send(result);
      }).catch((err) => {
        return service.serviceUtils.handleError(res, err);
      });
    };
  }

  iDelete(uuid, transaction) {
    let service = this;
    return service.Model.update(
      {
        deletedAt: new Date()
      },
      {
        where: { uuid: uuid },
        transaction: transaction ? transaction : null,
        returning: true,
      }
    ).then((result) => {
      return 'DELETED';
    });
  }

  uniq(transaction) {
    let service = this;
    return (req, res) => {
      service.iUniq(req.body.field, req.body.value, req.body.uuid, transaction).then((result) => {
        res.send({ count: result });
      }).catch((err) => {
        return service.serviceUtils.handleError(res, err);
      });
    };
  }

  iUniq(field, value, uuid, transaction) {
    if(!field || !value ) {
      // TODO: analyze this
      return Promise.resolve(); //Promise.reject({ status: 400, name: 'Bad Request', message: 'Не переданы поля field, value или uuid в теле запроса' });
    }

    if(!uuid) {
      uuid = 'SOMEFAKEUUIDFORSEARCH';
    }

    let service = this;
    return service.Model.count(
      service.buildWhere({
        softFilter: {
          [field]: { value: value, logic: 'caseinsensitive' },
          uuid: { value: uuid, logic: 'not' }
        },
        // attributes: [[service.Model.sequelize.fn('COUNT', service.Model.sequelize.col('uuid')), 'count']],
        transaction: transaction ? transaction : null
      }, service.Model)
    ).then((result) => {
      return result;
    });
  }

  iTransactional(callback, transaction) {
    if(transaction) {
      return callback(transaction);
    } else {
      return this.db.transaction(function (transaction) {
        return callback(transaction);
      });
    }
  }

  /*
   attributes
   softFilter: [
     fieldName: {
       value: fieldValue *
       type: fieldType [DATE, TEXT]
       logic: search logic [iLike, eq, not, caseinsensitive]
     }
   ]
   softFilterUseLogicOr - use OR condition instead default AND.
   softFilterStrictMatch - if FALSE use FULL EQUALS logic, otherwise use LIKE logic
   order
   limit
   offset
   transaction
  */
  
  processPayload(payload, model) {
    if(!payload) return { };

    // PREPARE MODEL SCOPE
    let modelScope = new Set(['defaultScope']);
    if (payload.scope) {
      if (Array.isArray(payload.scope)) {
        _.each(payload.scope, item => modelScope.add(item));
      } else {
        modelScope.add(payload.scope);
      }
    }
    payload.scope = Array.from(modelScope);

    // PREPARE FILTER
    if(payload.softFilter) {
      let sequelize = model.sequelize,
        conditions = [];
      _.each(payload.softFilter instanceof Object ? payload.softFilter : JSON.parse(payload.softFilter), (filter, filterName) => {
        let modelFilterName = '"' + model.name + '"."' + filterName + '"';

        // Set logic if needed
        if(payload.softFilterStrictMatch == 'true'){
          filter.logic = 'eq';
        }

        if(filter.type && filter.type.toUpperCase() == 'DATE') {
          if(!filter.startDate) {
            filter.startDate = moment.utc('1970-01-01T00:00:00.000Z').format();
          }
          if(!filter.endDate) {
            filter.endDate = moment.utc('3000-01-01T00:00:00.000Z').format();
          }

          if(filter.startDate == filter.endDate) {
            filter.endDate = moment.utc(filter.endDate).set('hour', 23).set('minute', 59).set('second', 59).format();
          }
          conditions.push({
            [filterName] : { 
              [sequelize.Op.between]: [filter.startDate, filter.endDate]
            }
          });
        } else {
          // Prepare column
          let conditionColumn = sequelize.cast(sequelize.col(modelFilterName), 'TEXT'),
            logic = null;

          // Choose logic
          if(filter.logic && filter.logic == 'not') {
            logic = sequelize.Op.not;
          } else if(filter.logic && filter.logic == 'eq') {
            logic = sequelize.Op.eq;
          } else if(filter.logic && filter.logic == 'caseinsensitive') {
            logic = sequelize.Op.iLike;
          } else {
            logic = sequelize.Op.iLike;
            filter.value = '%' + filter.value + '%';
          }

          conditions.push({ [filterName]: sequelize.where( conditionColumn, { [logic]: filter.value }) });
        }
      });

      payload.where = {};
      if(payload.softFilterUseLogicOr) {
        payload.where[sequelize.Op.or] = conditions;
      } else {
        payload.where[sequelize.Op.and] = conditions;
      }
    }

    delete payload.softFilter;
    delete payload.softFilterUseLogicOr;
    delete payload.softFilterStrictMatch;

    return payload;
  }

  buildPayload(params, req, method) {
    let payload = {};
    payload.scope = _.get(params, 'scope');
    payload.transaction = _.get(params, 'transaction');

    switch(method) {
      case 'all':
        // SET FILTER
        payload.filter = _.get(req, 'query.filter');
        payload.softFilter = _.get(req, 'query.softFilter');
        payload.softFilterStrictMatch = _.get(req, 'query.softFilterStrictMatch');

        // SET ORDER
        let order = _.get(req, 'query.order');
        if (order) {
          payload.order = [];
          if (order.startsWith('-')) {
            payload.order.push([order.substr(1), 'DESC']);
          } else {
            payload.order.push(order);
          }
        }

        // LIMIT AND OFFSET
        let pageSize = _.get(req, 'query.pageSize');
        if (pageSize) {
          let page = _.get(req, 'query.page') || 0;
          payload.offset = page * pageSize;
          payload.limit = pageSize;
        }

        break;
      case 'get':
        payload.softFilter = {
          uuid: { value: _.get(req, 'params.id') }
        };
        payload.softFilterStrictMatch = true;

        break;
      default:
        break;
    }

    return payload;
  }

  buildWhere(params, model) {
    let query = {};
    if(!params) return query;

    if(params.transaction){
      query.trasaction = params.transaction;
    }

    // PREPARE FILTER
    if(params.softFilter) {
      let  sequelize = model.sequelize,
        conditions = [];
      _.each(params.softFilter instanceof Object ? params.softFilter : JSON.parse(params.softFilter), (filter, filterName) => {
        let modelFilterName = '"' + model.name + '"."' + filterName + '"';

        // Set logic if needed
        if(params.softFilterStrictMatch == 'true' && !filter.logic){
          filter.logic = 'eq';
        }

        if(filter.type && filter.type.toUpperCase() == 'DATE') {
          if(!filter.startDate) {
            filter.startDate = moment.utc('1970-01-01T00:00:00.000Z').format();
          }
          if(!filter.endDate) {
            filter.endDate = moment.utc('3000-01-01T00:00:00.000Z').format();
          }

          if(filter.startDate == filter.endDate) {
            filter.endDate = moment.utc(filter.endDate).set('hour', 23).set('minute', 59).set('second', 59).format();
          }
          conditions.push({
            [filterName] : { 
              [sequelize.Op.between]: [filter.startDate, filter.endDate]
            }
          });
        } else {
          // Prepare column
          let conditionColumn = sequelize.cast(sequelize.col(modelFilterName), 'TEXT'),
            logic = null;

          // Choose logic
          if(filter.logic && filter.logic == 'not') {
            logic = sequelize.Op.not;
          } else if(filter.logic && filter.logic == 'eq') {
            logic = sequelize.Op.eq;
          } else if(filter.logic && filter.logic == 'caseinsensitive') {
            logic = sequelize.Op.iLike;
          } else {
            logic = sequelize.Op.iLike;
            filter.value = '%' + filter.value + '%';
          }

          conditions.push({ [filterName]: sequelize.where( conditionColumn, { [logic]: filter.value }) });
        }
      });

      query.where = {};
      if(params.softFilterUseLogicOr) {
        query.where[sequelize.Op.or] = conditions;
      } else {
        query.where[sequelize.Op.and] = conditions;
      }
    }

    // SET ORDER
    if(params.order) {
      if(params.order.startsWith('-')){
        query.order = [[params.order.substr(1, params.order.length), 'DESC']];
      } else {
        query.order = [params.order];
      }

    }
    
    // LIMIT AND OFFSET
    if(params.pageSize) {
      query.limit = params.pageSize;
    }
    if(params.page) {
      query.offset = params.page * params.pageSize;
    }
    
    // SET ATTRIBUTES
    if(params.attributes)
      query.attributes = params.attributes;
    
    return query;
  }

  clearModelItem(item) {
    let attributes = _.map(this.Model.attributes, (attribute) => {
      return attribute.fieldName;
    });
    _.each(item, (value, name) => {
      if(!_.includes(attributes, name)){
        delete item[name];
      }
    });
    return item;
  }
  
  checkHash(item, transaction) {
    let service = this;
    return service.Model.findOne({
      where: { id: item.id },
      transaction: transaction ? transaction : null
    }).then(function (result) {
      if (!result || item.forceApply) {
        return true;
      } else {
        if (result.dataValues && result.dataValues.hash == item.hash) {
          return result;
        } else {
          return Promise.reject({
            status: 409,
            name: 'Server Error',
            message: 'Save conflict'
          });
        }
      }
    });
  }
}

exports.DefaultService = DefaultService;