'use strict';

var DefaultService = require('./default.server.service').DefaultService,
  utils = require(LOCALVARS.utilsPath),
  _ = require('lodash');

class Service extends DefaultService {
  constructor(db) {
    let service = module.exports.service ? module.exports.service : module.exports.service = super(db.models.user, db.models.userMv, db);
    return service;
  }
  save() {
    return (req, res) => {
      let user = req.body,
        player = req.body.player,
        service = this;

      if((!user.id && !user.uuid) || (!user.isHashed && user.needToChangePwd)) {
        user.password = utils.hashPassword(user.password);
      }

      return service.iTransactional((transaction) => {
          return service.iSave(user, null, transaction).then(result => {
            if(player) {
              player.userId = result.getDataValue('id');
            } else {
              player = { userId: result.getDataValue('id') };
            }
            return service.playerService.iSave(player, null, transaction).then(plSaveResult => {
              res.send({ uuid: result.uuid });
            });
          });
      }).catch(err => {
        return utils.handleError(500, err, res);
      });
    };
  }
}
exports.Service = Service;
