'use strict';

/**
 * Module dependencies.
 */
let db = require('../component/db/db-manager.server.js').db,
  userService  = new (require('./user.server.service').Service)(db),
  utils  = require(LOCALVARS.utilsPath),
  _ = require('lodash');

let INCORRECT_LOGIN_OR_PASSWORD = { message: ' Wrong login or password' };

exports.login = function (req, res) {
  let credentials = req.body,
    userModel = db.models.user;
  if(!credentials.login && !credentials.password) {
    return utils.handleError(403, INCORRECT_LOGIN_OR_PASSWORD, res);
  } else {
    userModel.findOne({ where: {
      login: { [userModel.sequelize.Op.iLike]: credentials.login },
      password: credentials.isHashed ? credentials.password : utils.hashPassword(credentials.password)
    }}).then(user => {
      if(!user) {
        return utils.handleError(403, INCORRECT_LOGIN_OR_PASSWORD, res);
      } else {
        delete user.dataValues.password;
        // GET USER PERMISSIONS VIA ROLE
        return user.getRole().then(userRole => {
          // SAVE ROLE NAME
          user.setDataValue('roleName', userRole.getDataValue('name'));
          return userRole.getPermissions();
        }).then(rolePermissions => {
          // PREPARE PERMISSIONS MAP
          let permissions = {};
          _.each(rolePermissions, rp => {
            permissions[rp.code] = true;
          });
          return permissions;
        }).then(permissions => {
          // SAVE PERMISSIONS MAP
          user.setDataValue('permissions', permissions);
          // Save session user
          req.session.user = user.dataValues;
          // Send prepared user
          return res.send(user);
        });
      }
    }).catch(err => {
      return utils.handleError(403, INCORRECT_LOGIN_OR_PASSWORD, res);
    });
  }
};

exports.logout = function (req, res) {
  req.session.destroy(() => {
    res.redirect('/');
});
};

exports.register = function (req, res) {
  return userService.save()(req, res);
};

exports.loggedin = function (req, res) {
  res.send(req.session.user);
};
