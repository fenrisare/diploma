'use strict';

var DefaultService = require('./default.server.service').DefaultService,
  utils = require(LOCALVARS.utilsPath),
  _ = require('lodash');

class Service extends DefaultService {
  constructor(db) {
    return module.exports.service ? module.exports.service : module.exports.service = super(db.models.permission, db.models.permissionMv, db);
  }
  allHierarchy() {
    return (req, res) => {
      let service = this,
        permissionsMatrix = {};
      return service.iAll().then(result => {
        _.each(result.rows || [], permission => {
          let code = permission.getDataValue('code');
          _.set(permissionsMatrix, code, code);
        });
        return permissionsMatrix;
      }).then(matrix => {
        res.send(matrix);
      }).catch(err => {
        return utils.handleError(500, err, res);
      });
    };
  }
}
exports.Service = Service;
