// Set null value for use default configuration
var RuntimeConfig = {
  brandName: 'SEAD License Server',
  language: 'eng',
  languages: [
    { 
      code: 'eng',
      name: 'English'
    },
    {
      code: 'rus',
      name: 'Русский'
    }
  ],
  api: {
    protocol: null,
    host: null,
    port: 3005
  },
  deviceRedirect: ''
};