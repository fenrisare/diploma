'use strict';

var path = require("path");

if(global.LOCALVARS) {
  global.LOCALVARS.mainCfg = __filename;
} else {
  global.LOCALVARS = {
    mainCfg: __filename
  };
}

module.exports = {
  title: 'LICENSE GENERATOR',
  server: {
    port: process.env.APP_PORT || 3005,
    debugPort: process.env.APP_PORT || 4005,
  },
  getAsset: () => {
    if(process.env.NODE_ENV == 'production') { // Return assets path for production enviroment
      return path.resolve('configuration/server/assets/production.js');
    } else if(process.env.NODE_ENV == 'uat') {  // Return assets path for user acceptance enviroment
      return path.resolve('configuration/server/assets/uat.js');
    } else { // Return assets path for development enviroment
      return path.resolve('configuration/server/assets/development.js');
    }
  }
};