'use strict';

module.exports = {
  client: {
    lib: {
      js: [
        './node_modules/angular/angular.js',
        './node_modules/angular-animate/angular-animate.min.js',
        './node_modules/angular-aria/angular-aria.min.js',
        './node_modules/angular-material/angular-material.min.js',
        './node_modules/angular-material-time-picker/dist/md-time-picker.js',
        './node_modules/angular-messages/angular-messages.min.js',
        './node_modules/angular-resource/angular-resource.min.js',
        './node_modules/@uirouter/angularjs/release/angular-ui-router.min.js',
        './node_modules/angular-ui-notification/dist/angular-ui-notification.min.js',
        './node_modules/lodash/lodash.min.js',
        './node_modules/js-sha256/build/sha256.min.js',
        './node_modules/angular-sanitize/angular-sanitize.min.js',
        './node_modules/angular-material-data-table/dist/md-data-table.min.js',
        './node_modules/moment/min/moment.min.js',
        './node_modules/moment/min/moment-with-locales.min.js',
        './node_modules/angular-bind-html-compile/angular-bind-html-compile.min.js',        
        './node_modules/angular-translate/dist/angular-translate.min.js',
        './node_modules/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
        './node_modules/ng-material-datetimepicker/dist/angular-material-datetimepicker.min.js',
        
        './configuration/client/local.runtime.config.js',
      ],
      css: [
        './node_modules/angular-material/angular-material.min.css',
        './node_modules/mdi/css/materialdesignicons.min.css',
        './node_modules/angular-ui-notification/dist/angular-ui-notification.min.css',
        './node_modules/angular-material-data-table/dist/md-data-table.min.css',
        './node_modules/angular-material-time-picker/dist/md-time-picker.css',
        './node_modules/ng-material-datetimepicker/dist/material-datetimepicker.min.css',
      ]
    },
	app: {
      js: [
        './modules/client/*/**/*.js' 
      ],
      css: [
        './modules/client/*/**/*.css' 
      ]
	}
  },
  server: {
    sessionSecret: 'ebfa134c-c210-4180-9d85-bc13ae545cf9',
    routes: ['modules/server/*/routes/**/*.js'],
    models: ['modules/server/*/models/**/*.js']
  }
};