'use strict';

module.exports = {
  client: {
    lib: {
      js: [
        'dist/application.lib.js'
      ],
      css: [
        'dist/application.lib.css'
      ]
    },
	app: {
      js: [
        'dist/application.js'
      ],
      css: [
        'dist/application.css'
      ]
	}
  },
  server: {
    sessionSecret: '0951b82b-c031-40f2-b435-49ecc69e696e',
    routes: ['modules/server/*/routes/**/*.js'],
    models: ['modules/server/*/models/**/*.js']
  }
};