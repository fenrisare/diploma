"use strict";

// LIBS IMPORT
let express = require('express'),
  app = express(),
  path = require('path'),
  bodyParser = require('body-parser'),
  morgan = require('morgan'),
  cors = require('cors');

// INIT PROCESS LOCALS
global.LOCALVARS = {
  ROOT_PATH: __dirname
};

// TOOLS IMPORT
let utils = require('./modules/server/core/component/utils'),
  config = require('./configuration/server/main'),
  asset = require(config.getAsset()),
  routes = require('./modules/server/core/component/routes/routes.server'),
  dbManager = require('./modules/server/core/component/db/db-manager.server'),
  staticManager = require('./modules/server/core/component/static/static.server'),
  sessionManager = require('./modules/server/core/component/session/session.server'),
  middleware = require('./modules/server/core/component/middleware');

// DEFAULT LOGER CONFIG
app.use(morgan('dev')); 

// SETUP STATIC FOLDERS (ACCESSABLE FROM FRONTEND)
staticManager.init(app, express);

// POST QUERIES BODY PARSER SETTINGS
app.use(bodyParser.urlencoded({ 'extended':'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

app.use(cors({
  origin: true,
  credentials: true,
  // allowedHeaders: ['If-None-Match', 'Cookie', 'Accept', 'Accept-Language', 'Content-Language', 'Content-Type', 'User-Agent', 'Referer', 'Origin', 'Host', 'Connection', 'Accept-Encoding'],
  allowedMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  maxAge: 999999
}));

// INIT DB
dbManager.initDb(app, asset).then(db => {
  sessionManager.init(app, asset);

  // INIT ROUTES
  routes.init(app, db, asset);

  // DEFINE MAIN ROUTE & APP START
  routes.initMainRoute(app, asset);
  app.listen(config.server.port);
  console.log('===========================================');
  console.log('App started on port ' + config.server.port);
  console.log('Environment: ' + process.env.NODE_ENV);
  console.log('===========================================');
}).catch(err => {
  console.error(err);
  app.exit();
});

